const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')
const spanFullName2 = document.querySelector('#span-full-name2')

//keyup event is fired when a key is released

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtLastName.addEventListener('keyup', (event) => {
	spanFullName2.innerHTML = txtLastName.value;
})

/*Instructor's solution
	const updateFullName = () => {
		let firstName = txtFirstName.value
		let lastName = txtLastName.value

		spanFullName.innerHTML = `${firstName} ${lastName}`
	}

	txtFirstName.addEventListener('keyup', updateFullName)
	txtLastName.addEventListener('keyup', updateFullName)
*/